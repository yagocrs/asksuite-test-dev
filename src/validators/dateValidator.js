/**
 * Validates that the value is a valid date.
 * @param Date to validate.
 */
export function dateFormatValidator(date) {
  const dateFormat = /^\d{4}-\d{2}-\d{2}$/
  return dateFormat.test(date)
}

/**
 * Validates that the date is bigger or equal than today
 * @param Date to validate.
 */
export function dateIsBiggerOrEqualToday(date) {
  const today = __stringifyDate('')
  const dateToValidate = __stringifyDate(date)

  if (dateToValidate === today) return true

  return dateToValidate > today
}

/**
 * Return date YYYY-MM-DD stringified
 * @param Date to stringify.
 */
function __stringifyDate(date) {
  return !!date
    ? JSON.stringify(new Date(date)).split('T')[0].replace(/\"/, '')
    : JSON.stringify(new Date()).split('T')[0].replace(/\"/, '')
}
