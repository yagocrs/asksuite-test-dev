import 'dotenv/config'
import fetch from 'node-fetch'
import puppeteer from 'puppeteer'

// =-=-=-=-= Settings =-=-=-=-=
import { puppeteerSettings } from './browserSettings.js'

// =-=-=-=-= Env =-=-=-=-=
const envUrl = process.env.URL
const descUrl = process.env.URL_DESC

// =-=-=-=-= Validator =-=-=-=-=-=
import { dateFormatValidator, dateIsBiggerOrEqualToday } from '../../validators/dateValidator.js'

class BrowserService {
  //
  static run(checkin, checkout) {
    return new Promise((resolve, reject) => {
      !this.dateValidator([checkin, checkout])

      const dates = this.fixDatesFormat([checkin, checkout])
      const url = envUrl.replace('{checkin}', dates[0]).replace('{checkout}', dates[1])

      this.getRoomsData(url)
        .then((result) => resolve(result))
        .catch((err) => reject(err))
    })
  }

  //
  static getRoomsData(url) {
    return new Promise(async (resolve, reject) => {
      //
      const browser = await puppeteer.launch(puppeteerSettings)
      const page = await browser.newPage()
      await page.exposeFunction('getFullDescriptionPage', this.getFullDescriptionPage)
      await page.exposeFunction('reject', reject)
      await page.goto(url)

      await page
        .evaluate(async () => {
          // =-=-=-=-= Valid rooms =-=-=-=-=
          let validRooms = []
          document.querySelectorAll('.roomrate').forEach((card) => {
            if (card.classList.value.includes('d-none')) return
            validRooms.push(card)
          })

          // =-=-=-=-= No rooms =-=-=-=-=
          if (!validRooms.length)
            reject({
              code: 200,
              message: 'There is no room available.',
            })

          let roomsIds = []
          validRooms.forEach((room) => {
            const nodeList = room.querySelectorAll('.modal.fotorama-modal')
            const roomId = nodeList[0].classList.value.split(' ')[2].split('-')[2]
            roomsIds.push(roomId)
          })

          const completeDescriptions = await getFullDescriptionPage(roomsIds)

          let result = []
          validRooms.forEach((el) => {
            let roomObjModel = {}

            const roomId = el
              .querySelector('.modal.fotorama-modal')
              .classList.value.split(' ')[2]
              .split('-')[2]
            roomObjModel.id = roomId

            // =-=-=-=-= Room names =-=-=-=-=
            const name = el.querySelector('.custom-hotel-name').innerText
            !!name && (roomObjModel.name = name)

            // =-=-=-=-= Room images =-=-=-=-=
            const image = el.querySelector('.image-step2').getAttribute('src')
            !!image && (roomObjModel.image = image)

            const bigImage = !!image && image.replace('246x197', '960x640')
            !!bigImage && (roomObjModel.big_image = bigImage)

            // =-=-=-=-= Room prices =-=-=-=-=
            let prices = []
            el.querySelectorAll('.rate_plan').forEach((card) => {
              const priceName = card.querySelector('.t-tip__in').innerText
              const priceValue = card.querySelector('.price-total').innerText
              prices.push({ price_name: priceName, price_value: priceValue })
            })
            !!prices && (roomObjModel.prices = prices)

            // =-=-=-=-= Room descriptions =-=-=-=-=
            const slicedDescription = el.querySelector('.description.hotel-description').innerText
            !!slicedDescription &&
              (roomObjModel.description = slicedDescription.replace(' (ver mais)', ''))

            // =-=-=-=-= Max occupancy =-=-=-=-=
            const maxOccupancy = el.querySelectorAll('.float-right span:last-child')[0].innerText
            !!maxOccupancy && (roomObjModel['max_occupancy'] = maxOccupancy)

            result.push(roomObjModel)
          })

          //
          return { result, completeDescriptions: completeDescriptions }
        })
        .then((result) => {
          const final = this.joinResults(result)

          resolve(final)
          this.closeBrowser(browser)
        })
        .catch((err) => {
          reject({
            code: 500,
            message: `Something went wrong: ${err}.`,
          })
          this.closeBrowser(browser)
        })
    })
  }

  //
  static joinResults(resultsArray) {
    let roomsInfos = resultsArray.result

    resultsArray.completeDescriptions.forEach((desc) => {
      roomsInfos.forEach((result) => {
        if (result.id === desc.id) {
          delete result.id
          result.complete_description = desc.complete_description
        }
      })
    })

    return roomsInfos
  }

  //
  static closeBrowser(browser) {
    if (!browser) return
    return browser.close()
  }

  //
  static dateValidator(dates) {
    return dates.forEach((date) => {
      if (!dateIsBiggerOrEqualToday(date))
        throw { code: 400, message: 'Date should be equal or bigger than today.' }

      if (!dateFormatValidator(date))
        throw { code: 400, message: 'Invalid date. Please use format YYYY-MM-DD.' }
    })
  }

  //
  static fixDatesFormat(dates) {
    return dates.map((date) => date.replace(/-/g, '/').split('/').reverse().join(''))
  }

  //
  static getFullDescriptionPage(roomsIds) {
    return new Promise((resolve, reject) => {
      let arr = []

      roomsIds.forEach((id) => {
        const url = descUrl.replace('{room_id}', id)

        fetch(url)
          .then((res) => res.text())
          .then((body) => {
            const re = /"hotel-info">(.*?)<\/p>/g
            return body.match(re)[0].replace('"hotel-info">', '').replace('</p>', '')
          })
          .then((description) => {
            arr.push({ id: id, complete_description: description })
          })
          .catch((err) => reject(err))
          .finally((_) => {
            if (arr.length === roomsIds.length) resolve(arr)
          })
      })
    })
  }
}

export default BrowserService
