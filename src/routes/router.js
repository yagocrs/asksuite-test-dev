import express from 'express'
import BrowserService from '../services/Browser/BrowserService.js'

const router = express.Router()

router.get('/', (_, res) => {
  res.send('Hello Asksuite World!')
})

//TODO implement endpoint here
router.post('/search', (req, res) => {
  const { checkin, checkout } = req.body

  BrowserService.run(checkin, checkout)
    .then((rooms) => res.json(rooms))
    .catch((err) => res.json(err))
})

export default router
